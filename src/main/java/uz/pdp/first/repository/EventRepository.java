package uz.pdp.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.first.entity.Event;

public interface EventRepository extends JpaRepository<Event, Integer> {

//    @Modifying
    @Transactional
    @Query(value = "UPDATE event SET active = NOT active WHERE id=:id",nativeQuery = true)
    void updateChange(Integer id);
}
