package uz.pdp.first.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import uz.pdp.first.entity.Hall;

public interface HallRepository extends JpaRepository<Hall, Integer> {


}
