package uz.pdp.first.utils;

import uz.pdp.first.controller.AuthController;
import uz.pdp.first.controller.EventController;

public interface AppConstants {

    String[] OPEN_PAGES = {
            AuthController.BASE_PATH + "/**",
            EventController.BASE_PATH + EventController.GET_ALL
    };

    String AUTH_HEADER = "Authorization";
    String AUTH_TYPE_BASIC = "Basic ";
    String AUTH_TYPE_BEARER = "Bearer ";

    String BASE_PATH = "/api";
}
