package uz.pdp.first.controller;

import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.pdp.first.aop.PreAuthorize;
import uz.pdp.first.entity.Event;
import uz.pdp.first.entity.Hall;
import uz.pdp.first.exceptions.CardBlockedException;
import uz.pdp.first.exceptions.SmsSendException;
import uz.pdp.first.payload.EventDTO;
import uz.pdp.first.payload.HallDTO;
import uz.pdp.first.payload.PaginationDTO;
import uz.pdp.first.repository.EventRepository;
import uz.pdp.first.repository.HallRepository;
import uz.pdp.first.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(EventController.BASE_PATH)
public class EventController {

    public static final String BASE_PATH = AppConstants.BASE_PATH + "/event";
    public static final String GET_ALL = "/list";
    @Autowired
    EventRepository eventRepository;

    @Autowired
    HallRepository hallRepository;


    @PreAuthorize(roles = {"ADMIN", "MODER"})
    @PostMapping
    public boolean add(@RequestBody EventDTO eventDTO) {
        Event event = new Event(eventDTO.getName(),
                eventDTO.getDescription(),
                eventDTO.getMaxTicketCount(),
                hallRepository.findById(eventDTO.getHallId()).orElseThrow());
        eventRepository.save(event);
        return true;
    }


    @GetMapping(GET_ALL)
    public PaginationDTO<HallDTO> getAll(@RequestParam(defaultValue = "1") int page,
                                         @RequestParam(defaultValue = "20") int size) {

        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Event> eventPage = eventRepository.findAll(pageable);
        for (Event event : eventPage.getContent()) {
            System.out.println(event.getName() + " =>" + event.getHall().getName());
        }
        return new PaginationDTO<>();
    }

    private List<HallDTO> mapHallListToHallDTOList(List<Hall> halls) {
        List<HallDTO> hallDTOList = new ArrayList<>();
        for (Hall hall : halls) {
            HallDTO hallDTO = new HallDTO();
            hallDTO.setCapacity(hall.getCapacity());
            hallDTO.setLocation(hall.getLocation());
            hallDTO.setName(hall.getName());
            hallDTOList.add(hallDTO);
        }
        return hallDTOList;
    }

    @PatchMapping("/active/{id}")
    public String changeActive(@PathVariable Integer id) {

//        Event event = eventRepository.findById(id).orElseThrow(IllegalArgumentException::new);
//        event.setActive(!event.isActive());
//        eventRepository.save(event);
        eventRepository.updateChange(id);
        return "OK";
    }
}
