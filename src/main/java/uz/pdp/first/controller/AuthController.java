package uz.pdp.first.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.first.entity.User;
import uz.pdp.first.payload.SignDTO;
import uz.pdp.first.repository.UserRepository;
import uz.pdp.first.service.AuthService;
import uz.pdp.first.utils.AppConstants;

@RestController
@RequestMapping(AuthController.BASE_PATH)
@RequiredArgsConstructor
public class AuthController {

    public static final String BASE_PATH = AppConstants.BASE_PATH + "/auth";
    public static final String SIGN_UP_PATH = "sign-up";
    public static final String SIGN_IN_PATH = "sign-in";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthService authService;

    @PostMapping(SIGN_UP_PATH)
    public String signUp(@RequestBody @Valid SignDTO signDTO) {
        if (userRepository.existsByUsername(signDTO.getUsername()))
            return "Oka bunday username avval qo'shilgan";

        User user = new User(
                signDTO.getUsername(),
                passwordEncoder.encode(signDTO.getPassword())
        );
        userRepository.save(user);
        return "Ok okasi";
    }
    @PostMapping(SIGN_IN_PATH)
    public String signIn(@RequestBody @Valid SignDTO signDTO) {
        return authService.signIn(signDTO);
    }

}
