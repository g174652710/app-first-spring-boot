package uz.pdp.first.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.first.entity.Hall;
import uz.pdp.first.payload.HallDTO;
import uz.pdp.first.payload.PaginationDTO;
import uz.pdp.first.repository.HallRepository;
import uz.pdp.first.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(HallController.BASE_PATH)
@RequiredArgsConstructor
public class HallController {

    public static final String BASE_PATH = AppConstants.BASE_PATH + "/hall";
    private final HallRepository hallRepository;

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_YANGI')")
    @PostMapping
    public String add(@RequestBody HallDTO hallDTO) {
        Hall hall = new Hall();
        hall.setCapacity(hallDTO.getCapacity());
        hall.setLocation(hallDTO.getLocation());
        hall.setName(hallDTO.getName());
        hallRepository.save(hall);
        return "ll";
    }


    @GetMapping
    public PaginationDTO<HallDTO> getAll(@RequestParam(defaultValue = "1") int page,
                                         @RequestParam(defaultValue = "20") int size) {

        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Hall> hallPage = hallRepository.findAll(pageable);
        hallPage.get().forEach(hall -> {
            System.out.println(hall.getName());
        });

        return null;
//        return new PaginationDTO<>(
//                hallPage.getTotalPages(),
//                hallPage.getNumber() + 1,
//                hallPage.getSize(),
//                mapHallListToHallDTOList(hallPage.getContent()));
    }

    private List<HallDTO> mapHallListToHallDTOList(List<Hall> halls) {
        List<HallDTO> hallDTOList = new ArrayList<>();
        for (Hall hall : halls) {
            HallDTO hallDTO = new HallDTO();
            hallDTO.setCapacity(hall.getCapacity());
            hallDTO.setLocation(hall.getLocation());
            hallDTO.setName(hall.getName());
            hallDTOList.add(hallDTO);
        }
        return hallDTOList;
    }
}
