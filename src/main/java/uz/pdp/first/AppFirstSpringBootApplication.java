package uz.pdp.first;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import uz.pdp.first.entity.Hall;
import uz.pdp.first.repository.HallRepository;

@SpringBootApplication
@EnableTransactionManagement
public class AppFirstSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppFirstSpringBootApplication.class, args);
    }


}
