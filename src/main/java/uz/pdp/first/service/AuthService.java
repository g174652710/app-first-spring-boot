package uz.pdp.first.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import uz.pdp.first.controller.AuthController;
import uz.pdp.first.entity.User;
import uz.pdp.first.payload.SignDTO;
import uz.pdp.first.repository.UserRepository;
import uz.pdp.first.security.UserPrincipal;

import java.util.Date;

@Service
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;

    @Value("${app.jwt.token.key}")
    private String TOKEN_KEY;

    @Value("${app.jwt.token.expiration}")
    private Long TOKEN_EXPIRATION;

    public AuthService(UserRepository userRepository, @Lazy AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping(AuthController.SIGN_IN_PATH)
    public String signIn(@Valid @RequestBody SignDTO signDTO) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        signDTO.getUsername(),
                        signDTO.getPassword())
        );
        User user = ((UserPrincipal) authentication.getPrincipal()).getUser();


        //todo token generate qilish
        Date expirationDate = new Date(System.currentTimeMillis()
                + TOKEN_EXPIRATION);

        return Jwts
                .builder()
                .signWith(SignatureAlgorithm.HS256, TOKEN_KEY)
                .setSubject(user.getId().toString())
                .setIssuedAt(new Date())
                .setExpiration(expirationDate)
                .compact();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));
        return new UserPrincipal(user);
    }
}