package uz.pdp.first.aop;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface PreAuthorize {

    String[] roles() default {};
}
