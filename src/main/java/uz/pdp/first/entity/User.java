package uz.pdp.first.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.userdetails.UserDetails;
import uz.pdp.first.entity.enums.RoleEnum;

import java.util.Base64;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
public final class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private RoleEnum roleEnum;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
