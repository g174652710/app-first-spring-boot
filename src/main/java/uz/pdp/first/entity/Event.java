package uz.pdp.first.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    private String description;

    private Integer maxTicketCount;

    @ManyToOne(fetch = FetchType.LAZY)
    private Hall hall;

    private boolean active;

    public Event(String name, String description, Integer maxTicketCount, Hall hall) {
        this.name = name;
        this.description = description;
        this.maxTicketCount = maxTicketCount;
        this.hall = hall;
    }
}
