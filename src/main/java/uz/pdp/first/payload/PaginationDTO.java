package uz.pdp.first.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PaginationDTO<T> {

    private int totalPages;//6,6,6

    private int page;//1,2,6

    private int size;//20,20,20

    private List<T> content;
}
