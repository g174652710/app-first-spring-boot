package uz.pdp.first.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HallDTO {

    private String name;

    private Integer capacity;

    private String location;


}
