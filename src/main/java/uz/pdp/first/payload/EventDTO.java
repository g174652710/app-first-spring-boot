package uz.pdp.first.payload;

import lombok.Getter;
import uz.pdp.first.entity.Hall;

@Getter
public class EventDTO {

    private String name;

    private String description;

    private Integer maxTicketCount;

    private Integer hallId;



}
